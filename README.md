# GuitarTabs

A collection of my guitar tabs. Mostly video game music, from games like Final Fantasy, Legend of Dragoon, Stronghold, etc.

Most of the tabs are in .tg/.gp5 format, and can be opened in Tux Guitar (or Guitar Pro).

.tab files are simple ASCII files, and can be viewed in any text file viewer.
